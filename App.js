import React,{Component} from 'react';
import { View, Text, SafeAreaView } from 'react-native';
export default class App extends Component {
  render() {
    return(
      <SafeAreaView style={styles.container}>
        <View style={styles.rowContainer}>
          <View style={{flex: 0.5, backgroundColor: '#7fffd4'}} />
          <View style={{flex: 0.5, backgroundColor: '#ff1493'}} />
          </View>
        <View style={styles.columnContainer}> 
          <View style={{flex: 0.5, backgroundColor: '#2f4f4f'}} />
          <View style={{flex: 0.5, backgroundColor: '#00008b'}} />
          <View style={{flex: 0.5, backgroundColor: '#800000'}} />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = {
  container: {
    flex: 1
  },

  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ecf5fd',
  },

  columnContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf5fd',
  }
}
