import React from "react";
import{Text,View,TouchableOpacity, Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Storage = () => {
    
    const SetName = async()=>{
    await AsyncStorage.setItem('Name','Nandhini')
    }

    const GetName = async()=>{
    const name = await AsyncStorage.getItem('Name')
    Alert.alert('Name',name)
    
    } 
    
    return(
    <View style={{flex:1 ,alignItems:'center', justifyContent:'center'}}>

    <TouchableOpacity  onPress={SetName} style={{height:50 ,width: 100 , alignItems:'center',backgroundColor:'pink'}}>
    <Text style={{color:'black',fontSize: 30}}>Set</Text>
    </TouchableOpacity>
    <TouchableOpacity onPress={GetName} style={{ height:50 ,width: 100 , alignItems:'center',backgroundColor:'pink',marginTop: 20}}>
    <Text style={{color:'black',fontSize: 30}}>Get</Text>
    </TouchableOpacity>

    </View>
)
}
export default Storage;
