/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AppRouter from './src/Router/AppRouter';
import Assignment1 from './src/NandhiniAssignment1/Assignment1';


AppRegistry.registerComponent(appName, () => Assignment1);
