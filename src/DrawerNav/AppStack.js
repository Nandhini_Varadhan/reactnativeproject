import React from "react";
import Profile from '../Design/Profile';
import File from '../Design/File'
import Bell from '../Design/Bell';
import HomeScreen from '../Design/HomeScreen';
import { createDrawerNavigator } from "@react-navigation/drawer";
import CustomDrawer from './CustomDrawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const Drawer = createDrawerNavigator();

const AppStack =()=>{
    return(
        
        <Drawer.Navigator  initialRouteName="Home" drawerContent= { props => <CustomDrawer{...props}/>} screenOptions ={{headerShown:false,drawerActiveBackgroundColor:'#B80857',drawerActiveTintColor:'#fff',drawerInactiveBackgroundColor:'#000',drawerLabelStyle:{color:'#fff',marginLeft:-25,fontFamily:'Roboto-Medium',fontSize:15}}}>
         <Drawer.Screen
                name = "Home"
                component ={HomeScreen}
                options={{drawerIcon:() => (
                    <Ionicons name = "home" size={22} color={'#fff'}/>
                )
                }}
            />            
        <Drawer.Screen
                name = "File"
                component ={File}
                options={{drawerIcon:() => (
                    <FontAwesome5 name = "file" size={22} color={'#fff'}/>
                )
                }}
            />
            <Drawer.Screen
                name = "Profile"
                component ={Profile}
                options={{drawerIcon:() => (
                    <Ionicons name = "person" size={22} color={'#fff'}/>
                )
                }}
            />
            <Drawer.Screen
                name = "Notification"
                component ={Bell}
                 options={{drawerIcon:() => (
                    <Ionicons name = "notifications" size={22} color={'#fff'}/>
                )
                }}
            />
        </Drawer.Navigator>
        
    );
};
export default AppStack;