import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  SafeAreaView,
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CustomDrawer = props => {
  return (
    <View style={{flex: 1, backgroundColor: '#000'}}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{backgroundColor: '#000'}}
      >
        <ImageBackground
          source={require ('../../assets/black.jpg')}
          style={{padding: 10}}
        >
          <Image
            source={require ('../../assets/nandy.jpg')}
            style={{
              height: 80,
              width: 80,
              borderRadius: 40,
              marginBottom: 10,
              marginTop: 50,
              borderColor: '#fff',
              borderWidth: 2,
            }}
          />
          <Text
            style={{color: '#fff', fontSize: 18, fontFamily: 'Roboto-Medium'}}
          >
            Nandhini
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#fff',
                fontFamily: 'Roboto-Regular',
                marginBottom: 5,
              }}
            >
              2M followers
            </Text>
            <Ionicons
              name="person"
              color={'#fff'}
              size={16}
              style={{marginBottom: 10, marginStart: 4}}
            />
          </View>
        </ImageBackground>
        <View
          style={{
            flex: 1,
            backgroundColor: '#000',
            paddingTop: 10,
            borderTopWidth: 1,
            borderTopColor: '#ccc',
          }}
        >
          <DrawerItemList {...props} />
        </View>
      </DrawerContentScrollView>
      <View style={{padding: 20, borderTopWidth: 1, borderTopColor: '#ccc'}}>
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="exit-outline" size={22} color={'#fff'} />
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Roboto-Medium',
                marginLeft: 5,
                color: '#fff',
              }}
            >
              Sign Out
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default CustomDrawer;
