import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AppStack from "./AppStack";

const stack = createNativeStackNavigator();

function DrawerRouter(){
    return(
        <NavigationContainer>
            <AppStack/>
        </NavigationContainer>
    );
}
export default DrawerRouter;