import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';



const DATA = [
  {name: 'A'},
  {name: 'B'},
  {name: 'C'},
  {name: 'D'},
  {name: 'E'},
  {name: 'F'},
  {name: 'G'},
  {name: 'H'},
  {name: 'I'},
  {name: 'J'},
  {name: 'K'},
  {name: 'L'},
  {name: 'M'},
  {name: 'N'},
  {name: 'O'},
  {name: 'P'},
  {name: 'Q'},
  {name: 'R'},
];

const Assignment1 = () => {

   const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getPage = async () => {
     try {
      const response = await fetch('https://reqres.in/api/users?page=1');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getPage();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flexDirection: 'row', marginStart: 10}}>
        <MaterialIcons name="clear-all" size={60} color={'#E55C18'} />
        <Text
          style={styles.profile}
        >
          Profile Viewer
        </Text>
      </View>
      <View style={{backgroundColor: '#EFEFF4'}}>
        <View
          style={styles.search}
        >
          <FontAwesome
            name="search"
            size={20}
            style={{fontSize: 20, marginStart: 120, padding: 6}}
          />
          <Text style={styles.textSearch}>
            Search
          </Text>
        </View>
      </View>
      <View style={{backgroundColor: '#F2F1F1'}}>
        <View style={{flexDirection: 'row', marginTop: 18}}>
          <Text
            style={styles.recent}
          >
            Recent Viewer
          </Text>
          <View
            style={styles.number}
          >
            <Text style={{alignSelf: 'center', padding: 3, color: '#FFFFFF'}}>
              729
            </Text>
          </View>
          <AntDesign name="bars" size={30} style={{marginStart: 125}} />
          <Feather name="grid" size={30} style={{marginStart: 10}} />
        </View>
        <Text
          style={styles.line}
        />
        <View style={{flexDirection: 'row'}}>
          <View style={{flex:1,elevation: 8,shadowOpacity: 0.8}}>
          {isLoading ? <ActivityIndicator size={50} color={'#B8B8B8'}/> : (
            <FlatList
              flex ={1}
              numColumns={2}
              data={data.data}
              renderItem={({item}) => (
                <View style={[styles.column, {flexDirection: 'column'}]}>
                  <Image
                    style={styles.image}
                    source={{uri: item.avatar}}
                  />
                  <Text
                    style={styles.data}
                  >
                    {item.first_name} {item.last_name}
                  </Text>
                  <Text
                    style={{ fontSize: 12, color: '#000'}}
                  >
                    {item.email}
                  </Text>
                </View>
              )}
            />
          )}
          </View>
          <View style={{flexDirection: 'column'}}>
            <View style={{marginStart: 10, width: 30, alignItems: 'center'}}>
              <FlatList
                data={DATA}
                renderItem={({item}) => (
                  <View style={{alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>{item.name}</Text>
                  </View>
                )}
              />
            </View>
          </View>
        </View>
      </View>
      <View
        style={styles.ionicon}
      >
        <Ionicons
          name="md-home"
          size={35}
          Text="Home"
          style={{marginTop: 20}}
        />
        <Ionicons
          name="ios-eye-outline"
          size={35}
          style={[styles.ioniconStyle,{color: '#E55C18'}]}
        />

        <Ionicons
          name="people"
          size={35}
          style={styles.ioniconStyle}
        />
        <FontAwesome
          name="comments-o"
          size={35}
          style={styles.ioniconStyle}
        />
        <Ionicons
          name="settings"
          size={35}
          style={styles.ioniconStyle}
        />
      </View>
      <View>
        <Text
          style={styles.endLine}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  profile:{
    marginStart: 60,
    alignSelf: 'center',
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 26,
  },
  search:{
    flexDirection: 'row',
    alignSelf: 'center',
    borderRadius: 4,
    marginTop: 10,
    marginBottom: 10,
    width: 370,
    height: 36,
    backgroundColor: '#fff',
  },
  textSearch:{
    padding: 3, 
    fontSize: 20, 
    fontWeight: '500',
  },
  recent:{
    fontSize: 18,
    color: '#000',
    marginStart: 20,
    fontWeight: '600',
  },
  number:{
    marginStart: 8,
    backgroundColor: '#E55C18',
    width: 40,
    height: 26,
    borderRadius: 25,
  },
  line:{
    marginTop: 16,
    borderTopWidth: 1,
    width: 360,
    alignSelf: 'center',
    borderColor:'#B8B8B8'
  },
  data:{
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  ionicon:{
    flexDirection: 'row',
    marginStart: 40,
    marginEnd: 20,
    marginBottom: 20,
  },
  ioniconStyle:{
    marginStart: 40, 
    marginTop: 20
  },
  endLine:{
    borderTopWidth: 6,
    borderColor: '#E55C18',
    width: 150,
    borderRadius: 1,
    alignSelf: 'center',
  },        
  column: {
    height: 150,
    marginStart: 26,
    marginBottom: 20,
    width: 160,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems:'center'
  },
  image: {
    marginTop: 20,
    height: 70,
    width: 70,
    borderRadius: 40,
    borderWidth: 2,
    borderColor: '#ffffff',
  },
});

export default Assignment1;
