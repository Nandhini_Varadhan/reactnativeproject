import React, { useEffect, useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image,TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';

const Flatlist = () => {

    const Update = () => {
       fetch('https://jsonplaceholder.typicode.com/posts',{
       method: 'PUT',
       body: JSON.stringify({

         title:'foo',
         body:'bar',
       }),
        headers: { "Content-Type": "application/json; charset= utf-8" },
        })
       .then(response => response.json())
       .then(flatlist => setData(flatlist)
       );
    };

    const Delete = () => {
       fetch('https://jsonplaceholder.typicode.com/posts/0',{
         method: 'DELETE'
       })
    }

     const [data,setData]= useState([])
     
     useEffect(()=>{
       fetch('https://jsonplaceholder.typicode.com/posts')
       .then(response => response.json())
       .then(flatlist => setData(flatlist)
       );
     },[]);

    return(
      
      <SafeAreaView style={styles.container}>
        <FlatList
        data ={data}
        renderItem={({item}) =>(
          <View style={[styles.item,{flexDirection:'row'}]}>
            <View>
            <Text style={{fontSize:30,fontWeight:'bold',color:'black'}}>Title:{item.title}</Text>
            <Text style={{fontSize:28,fontWeight:'bold',color:'black',marginTop: 5}}>Body:{item.body}</Text>
            </View>
          </View>
        )}
        />
        <View style={{flexDirection : 'row'}}>
        <TouchableOpacity>
              <Text
                style={[
                  styles.button,
                  {
                    color: '#ffffff',
                    backgroundColor: '#00008b',
                    margin: 5,
                    marginTop: 20,
                    marginBottom: 20,
                    marginStart: 20,
                  },
                ]} 
                onPress= {Update}
                activeOpacity={0.4}
                >
                Update
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text
                style={[
                  styles.button,
                  {
                    color: '#ffffff',
                    backgroundColor: '#00008b',
                    margin: 5,
                    marginTop: 20,
                    marginBottom: 20,
                    marginStart: 5,
                  },
                ]} 
                onPress={Delete}
                activeOpacity={0.4}
                >
                Delete
              </Text>
            </TouchableOpacity>
            </View>
      </SafeAreaView>
    );
  }


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor:'white',
    padding: 20,
    borderColor: 'black',
    borderWidth: 3,
    borderRadius: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  button: {
    borderRadius: 5,
    borderWidth: 2,
    textAlign: 'center',
    fontSize: 20,
    fontWaight: 900,
    height: 40,
    width: 180,
    padding: 5,
    borderColor: '#00008b',
  },
});
export default Flatlist;
