import React from 'react';
import {TouchableOpacity, StyleSheet, View, Text, Image, SafeAreaView} from 'react-native';
 
const Hello = ({navigation}) =>{

    return(
    <SafeAreaView style={styles.container}>
    <View style={styles.boxContainer}>
       <Image
         style={{height:350, width:350}}
         source={require('../../assets/img.png')}
        />
     </View>

    <View style={styles.boxContainer}>
    <Text style={[styles.text, {fontSize: 40, fontWaight: '900'}]}>Hello !</Text>
    <Text style={[styles.text, {fontSize: 14, margin: 10}]}>Best place to write life stories and {'\n'}share your journey experiences</Text>
    <TouchableOpacity onPress = { () => navigation.navigate('Login')}>
    <Text style = {[styles.button,{color: '#ffffff',backgroundColor: '#00008b',margin: 5,marginTop: 40}]}>LOGIN</Text>
    </TouchableOpacity>
    <TouchableOpacity onPress = {() => navigation.navigate('Signup')}>
    <Text style = {[styles.button,{color: '#00008b',margin: 5}]}>SIGNUP</Text>
    </TouchableOpacity>
    </View>  
    </SafeAreaView> 
    );
  }

const styles =StyleSheet.create({
  container: {
    flex: 1,
  },
  boxContainer: {
      flex: 1,
      backgroundcolor: '#ffffff',
      alignItems: 'center', 
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    color: '#000000',
  },
  button: {
      borderRadius: 5,
      borderWidth: 2,
      textAlign:'center',
      fontSize: 20,
      height: 40,
      width:200,
      padding:5,
      borderColor: '#00008b',    
  }
});

export default Hello;
