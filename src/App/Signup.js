import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  TextInput,
  SafeAreaView,
  Image,
  Alert,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';

const Signup = ({navigation}) => {

   const[nameText,setNameText]=React.useState('') 
   const[emailText,setEmailText]=React.useState('')
   const[passwordText,setPasswordText]=React.useState('')

   const validation =()=>{

    if(nameText === ''){
      Alert.alert('Enter your Name')
    }
    else if(emailText === ''){
      Alert.alert('Enter your Email')
    }
    else if(passwordText === ''){
      Alert.alert('Enter your Password')
    }
    else{
      auth()
      .createUserWithEmailAndPassword(emailText, passwordText)
      .then(() => {
      navigation.navigate('Flatlist');
      })
      .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
      console.log('That email address is already in use!');  
      Alert.alert('That email address is already in use!');
      }
      if (error.code === 'auth/invalid-email') {
      console.log('That email address is invalid!');
      Alert.alert('That email address is invalid!');
      }
      console.error(error);
    })
    }
  }
    return (
      <KeyboardAwareScrollView>
        <SafeAreaView style={styles.container}>
          <View>
            <View style={{position: 'absolute', marginLeft: 10}}>
              <AntDesign name="arrowleft" size={40} color="black" />
            </View>
            <Text
              style={[
                styles.text,
                {fontSize: 40, fontWeight: 'bold', marginTop: 80},
              ]}
            >
              Hi!
            </Text>
            <Text style={[styles.text, {fontSize: 22, marginTop: 10}]}>
              Create a new account
            </Text>
            <TextInput
              style={[
                styles.text,
                {
                  height: 50,
                  width: 300,
                  fontWeight: 'bold',
                  marginTop: 60,
                  fontSize: 20,
                  marginStart: 30,
                },
              ]}
              onChangeText={(text) => setNameText(text)}
              placeholder="Enter name"
              borderBottomWidth={1}
            />
            <TextInput
              style={[
                styles.text,
                {
                  height: 50,
                  width: 300,
                  fontWeight: 'bold',
                  marginTop: 30,
                  fontSize: 20,
                  marginStart: 30,
                },
              ]}
              onChangeText={(text) => setEmailText(text)}
              placeholder="Enter email"
              borderBottomWidth={1}
            />
            <TextInput
              secureTextEntry={true}
              style={[
                styles.text,
                {
                  height: 50,
                  width: 300,
                  fontWeight: 'bold',
                  marginTop: 30,
                  fontSize: 20,
                  marginStart: 30,
                },
              ]}
              onChangeText={(text) => setPasswordText(text)}
              placeholder="Enter password"
              borderBottomWidth={1}
            />
            <TouchableOpacity>
              <Text
                style={[
                  styles.button,
                  {
                    color: '#ffffff',
                    backgroundColor: '#00008b',
                    margin: 5,
                    marginTop: 60,
                    alignSelf: 'center',
                  },
                ]}
                onPress={validation}
              >
                SIGN UP
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 20,
                marginTop: 40,
                alignSelf: 'center',
                color: 'black',
              }}
            >
              Social Media Signup{' '}
            </Text>
          </View>
          <View style={styles.boxContainer}>
            <Image
              style={{height: 30, width: 30}}
              source={require ('../../assets/google.png')}
            />
          </View>
          <Text
            style={{
              alignSelf: 'center',
              fontSize: 16,
              marginTop: 30,
              color: 'black',
              marginBottom: 20,
            }}
          >
            <Text>
              Already have an account?
            </Text>
            <Text
              onPress={() => navigation.navigate ('Login')}
              style={styles.highlight}
            >
              {' '}Sign in
            </Text>
          </Text>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }


const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },

  text: {
    marginStart: 30,
    color: 'black',
  },
  button: {
    borderRadius: 5,
    borderWidth: 2,
    textAlign: 'center',
    fontSize: 20,
    fontWaight: 900,
    height: 40,
    width: 200,
    padding: 5,
    borderColor: '#00008b',
  },
  boxContainer: {
    backgroundcolor: '#ffffff',
    alignSelf: 'center',
    marginTop: 10,
  }, 
  highlight: {
    color: '#00008b',
    fontWeight: 'bold',
  },
});

export default Signup;

