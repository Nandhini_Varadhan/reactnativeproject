import React from 'react';
import Zocial from 'react-native-vector-icons/Zocial';
import {ImageBackground, Text} from 'react-native';

const SplashScreen = ({navigation}) => {
setTimeout(()=>{
    navigation.navigate('Hello');
    },3000);
 return(
     <ImageBackground style={{flex: 1,alignItems:'center'}} source={require('../../assets/black.jpg')}>
     <Zocial name="plancast" size={80} style={{color:'#ffffff',marginTop:300}}/>
     <Text style={{color:'#C0C0C0',fontSize: 24,fontWeight:'300',marginTop: 300}}>from</Text>
     <Text style={{color:'#B80857', fontSize: 30,fontWeight:'500',fontStyle:'italic'}}>PENGUIN</Text>
     </ImageBackground>
 );   
 }
 
export default SplashScreen;