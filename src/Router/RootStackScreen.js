import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Hello from '../App/Hello'
import Login from '../App/Login'
import Signup from '../App/Signup'
import Flatlist from '../Flatlist'
import SplashScreen from '../App/SplashScreen';


const RootStack = createNativeStackNavigator ();

const RootStackScreen = () => (
    <RootStack.Navigator screenOptions ={{headerShown: false}}>
        <RootStack.Screen name ="SplashScreen" component={SplashScreen}/>
        <RootStack.Screen name ="Hello" component = {Hello}/>
        <RootStack.Screen name ="Login" component = {Login}/>
        <RootStack.Screen name ="Signup" component = {Signup}/>
        <RootStack.Screen name ="Flatlist" component = {Flatlist}/>
    </RootStack.Navigator>
);

export default RootStackScreen;