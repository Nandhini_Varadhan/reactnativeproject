import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootStackScreen from '../Router/RootStackScreen'

const AppRouter = () => {

    return(

        <NavigationContainer>
            <RootStackScreen/>
        </NavigationContainer>
    );
};

export default AppRouter;