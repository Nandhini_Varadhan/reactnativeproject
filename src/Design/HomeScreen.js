import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { View ,Text,TouchableOpacity} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const HomeScreen = ({navigation})=>{
    return(
    <View>
        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
        <FontAwesome5 name="bars" size={30} style={{alignSelf:'flex-start',padding: 10,color:'#000'}}/>
        </TouchableOpacity>
        <Text style={{alignSelf:'center',marginTop: 300,fontSize: 40,fontWeight:'bold',color:'#000'}}>Home</Text>
    </View>
    );
};
export default HomeScreen;