import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Main from './Main';

const stack = createNativeStackNavigator();

const BottomBarRouter = () => {

    return(

        <NavigationContainer>
            <Main/>
        </NavigationContainer>
    );
};

export default BottomBarRouter;