import React from 'react';
import {View, Text, SafeAreaView, Image, StyleSheet} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const File = () => {

  return (
    <KeyboardAwareScrollView>
      <SafeAreaView style={styles.container}>
        <View>
          <View style={{margin: 10, flexDirection: 'row', marginTop: 20}}>
            <Feather name="bar-chart" size={50} color="#ffffff" />
            <Image
              style={{
                height: 50,
                width: 50,
                borderRadius: 30,
                marginStart: 280,
                borderWidth: 2,
                borderColor: '#ffffff',
              }}
              source={require ('../../assets/nandy.jpg')}
            />
          </View>
          <View style={{marginStart: 30}}>
            <Text
              style={{
                fontSize: 30,
                color: '#ffffff',
                fontWeight: 'bold',
                marginTop: 20,
              }}
            >
              Hi Nandhini
            </Text>
            <Text style={{color: 'azure', fontSize: 18}}>
              6 Tasks are pending
            </Text>
          </View>
          <View
            style={[
              styles.paragraph,
              {
                flexDirection: 'column',
                height: 130,
                width: 350,
                marginTop: 30,
                alignSelf: 'center',
              },
            ]}
          >
            <Text style={{color: '#ffffff', paddingStart: 20, paddingTop: 10}}>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                Mobile App Design{'\n'}
              </Text>
              <Text style={{fontSize: 16}}>
                Mike and Anita{' '}
              </Text>
            </Text>
            <View style={{flexDirection: 'row', marginTop: 14}}>
              <Image
                style={{
                  height: 40,
                  width: 40,
                  borderRadius: 30,
                  marginStart: 20,
                  borderWidth: 2,
                  borderColor: '#ffffff',
                }}
                source={require ('../../assets/nandy.jpg')}
              />
              <Image
                style={{
                  height: 40,
                  width: 40,
                  borderRadius: 30,
                  borderWidth: 2,
                  borderColor: '#ffffff',
                  marginStart: -13,
                }}
                source={require ('../../assets/nandy.jpg')}
              />
              <Text
                style={{
                  color: '#ffffff',
                  marginStart: 200,
                  alignSelf: 'center',
                  fontSize: 16,
                }}
              >
                Now
              </Text>
            </View>
          </View>
          <View style={{marginStart: 30, marginTop: 30, flexDirection: 'row'}}>
            <Text
              style={{
                flex: 1,
                fontSize: 24,
                color: '#ffffff',
                fontWeight: 'bold',
              }}
            >
              Monthly Review
            </Text>
            <View
              style={{
                height: 40,
                width: 40,
                borderRadius: 30,
                backgroundColor: 'aqua',
                marginEnd: 20,
                padding: 8,
                alignSelf: 'center',
              }}
            >
              <MaterialCommunityIcons
                name="calendar-range"
                color="#ffffff"
                size={25}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row',marginBottom: 80}}>
            <View style={{flexDirection: 'column', flex: 1}}>
              <View
                style={[
                  styles.paragraph,
                  styles.box1,
                  {marginStart: 20, marginEnd: 10, marginTop: 30},
                ]}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 28,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  22
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginTop: 8,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  Done
                </Text>
              </View>
              <View
                style={[
                  styles.paragraph,
                  styles.box2,
                  {marginStart: 20, marginEnd: 10, marginTop: 20},
                ]}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 28,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  10
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginTop: 8,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  Ongoing
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'column', flex: 1}}>
              <View
                style={[
                  styles.paragraph,
                  styles.box2,
                  {marginStart: 10, marginEnd: 20, marginTop: 32},
                ]}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 28,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  7
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginTop: 8,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  In progress
                </Text>
              </View>
              <View
                style={[
                  styles.paragraph,
                  styles.box1,
                  {marginStart: 10, marginEnd: 20, marginTop: 20},
                ]}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 28,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  12
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginTop: 8,
                    textAlign: 'center',
                    color: '#ffffff',
                  }}
                >
                  Waiting for Review
                </Text>
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#5B0CB8',
  },
  paragraph: {
    backgroundColor: '#6F16D8',
    borderRadius: 20,
    color: '#ffffff',
  },
  box1: {
    flexDirection: 'column',
    height: 150,
    padding: 40,
  },
  box2: {
    flexDirection: 'column',
    height: 100,
    paddingHorizontal: 40,
    paddingTop: 10,
  },
});

export default File;
