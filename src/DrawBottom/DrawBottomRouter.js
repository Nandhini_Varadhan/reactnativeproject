import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import CustomDrawer from '../DrawerNav/CustomDrawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { NavigationContainer } from "@react-navigation/native";
import DrawBottomRoot from '../DrawBottom/DrawBottomRoot';
import HomeStackScreen from './DrawBottomRoot';
import FileStackScreen from './DrawBottomRoot';
import ProfileStackScreen from './DrawBottomRoot';
import BellStackScreen from './DrawBottomRoot';


const Drawer = createDrawerNavigator();

const DrawBottomRouter =()=>{
    return(
        <NavigationContainer>
        <Drawer.Navigator component={DrawBottomRoot}  initialRouteName="Home" drawerContent= { props => <CustomDrawer{...props}/>} screenOptions ={{headerShown:false,drawerActiveBackgroundColor:'#B80857',drawerActiveTintColor:'#fff',drawerInactiveBackgroundColor:'#000',drawerLabelStyle:{color:'#fff',marginLeft:-25,fontFamily:'Roboto-Medium',fontSize:15}}}>
         <Drawer.Screen
                name = "Home"
                component ={HomeStackScreen}
                options={{drawerIcon:() => (
                    <Ionicons name = "home" size={22} color={'#fff'}/>
                )
                }}
            />         
        <Drawer.Screen
                name = "File"
                component ={FileStackScreen}
                options={{drawerIcon:() => (
                    <FontAwesome5 name = "file" size={22} color={'#fff'}/>
                )
                }}
            />
            <Drawer.Screen
                name = "Profile"
                component ={ProfileStackScreen}
                options={{drawerIcon:() => (
                    <Ionicons name = "person" size={22} color={'#fff'}/>
                )
                }}
            />
            <Drawer.Screen
                name = "Notification"
                component ={BellStackScreen}
                options={{drawerIcon:() => (
                    <Ionicons name = "notifications" size={22} color={'#fff'}/>
                )
                }}
            />
        </Drawer.Navigator>
        </NavigationContainer>
        
    );
};
export default DrawBottomRouter;