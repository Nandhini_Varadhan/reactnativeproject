import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';

import File from '../Design/File';
import Profile from '../Design/Profile';
import Bell from '../Design/Bell';
import HomeScreen from '../Design/HomeScreen';

const HomeStack = createNativeStackNavigator();
const FileStack = createNativeStackNavigator();
const ProfileStack = createNativeStackNavigator();
const BellStack = createNativeStackNavigator();


const Tab = createMaterialBottomTabNavigator();

const DrawBottomRoot = () =>(
    
    <Tab.Navigator
    initialRouteName ="Home"
    activeColor="#ffffff"
    barStyle={{backgroundColor:'#5B0CB8'}}
    >
    <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
            tabBarLabel:'Home',
            tabBarColor: '#5B0CB8',
            tabBarIcon: ({ color }) => (
                <Feather name ="home" color={color} size={26}/>
            ),
        }}
    />    
    <Tab.Screen
        name="File"
        component={FileStackScreen}
        options={{
            tabBarLabel:'File',
            tabBarColor: '#5B0CB8',
            tabBarIcon: ({ color }) => (
                <Fontisto name ="file-1" color={color} size={26}/>
            ),
        }}
    />
    <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
            tabBarLabel:'Account',
            tabBarColor: '#5B0CB8',
            tabBarIcon: ({ color }) => (
                <Ionicons name ="person-outline" color={color} size={26}/>
            ),
        }}
    />  
    <Tab.Screen
        name="Bell"
        component={BellStackScreen}
        options={{
            tabBarLabel:'Notification',
            tabBarColor: '#5B0CB8',
            tabBarIcon: ({ color }) => (
                <Ionicons name ="notifications-outline" color={color} size={26}/>
            ),
        }}
    />
    </Tab.Navigator>
    
)
export default DrawBottomRoot;

const HomeStackScreen = () => (
    <HomeStack.Navigator screenOptions ={{headerShown: false}}>
        <HomeStack.Screen name ="Home" component={HomeScreen}/>
        </HomeStack.Navigator>
)

const FileStackScreen = () => (
    <FileStack.Navigator screenOptions ={{headerShown: false}}>
        <FileStack.Screen name ="File" component={File}/>
        </FileStack.Navigator>
)
const ProfileStackScreen = () => (
    <ProfileStack.Navigator screenOptions ={{headerShown: false}}>
        <ProfileStack.Screen name ="Profile" component={Profile}/>
        </ProfileStack.Navigator>
)
const BellStackScreen = () => (
    <BellStack.Navigator screenOptions ={{headerShown: false}}>
        <BellStack.Screen name ="Bell" component={Bell}/>
        </BellStack.Navigator>
)



